package Project1.keywords.common

public class dataHelper{
	
	static readJsonFile(String relativePath){
		...
	}

	def readDataConfiguration(String relativePath = "//configuration.json"){
		return readFileJson(relativePath)
	}

	def readDataDriven(String fileType){
		switch(fileType):
			case json:
				...
				break;
			case excel:
				...
				break;
			default:
	}
}