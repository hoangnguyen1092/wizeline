import actionHelper

class Login {
	
	// Page Object Elements
	testObject txt_Username = "//*[@id="login-page"]//input[@placeholder="Username"]"
	testObject txt_Password = "//*[@id="login-page"]//input[@placeholder="Password"]"
	testObject btn_Login = "//*[@id="login-page"]//button[@text="Login"]"
	
	// Actions
	static LoginPage login(String username, String password) {
		actionHelper.sendKeys(txt_Tenant, username)
		actionHelper.sendKeys(txt_Email, password)
		actionHelper.click(btn_Login)
		CommonAction.waitForElementClickable(HomePage.lnk_Logoff)
		return INSTANCE
	}
}